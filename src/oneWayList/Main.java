package oneWayList;

public class Main {
    public static void main(String[] args) {
        MyList head = new MyList(null, "Head", 0);

        head.addToList("First Object");
        head.addToList("Second Object");
        head.addToList("Third Object");
        head.addToList("Fourth Object");
        head.addToList("Fifth Object");
        head.addToList("Sixth Object");
        head.addToList("Seventh Object");
        head.addToList("Eight Object");
        head.addToList("Ninth Object");

        head.remove(3);

        head.remove(6);

        head.addInTheMiddle("3rd Object", 3);
        head.addInTheMiddle("11th Object", 11);


        head.showList();
    }

}
