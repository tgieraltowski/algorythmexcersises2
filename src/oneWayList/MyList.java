package oneWayList;

public class MyList {
    private static int counter = 1;

    MyList nextObject;
    String text;
    int index;



    public MyList(MyList nextObject, String text) {
        this.nextObject = nextObject;
        this.text = text;
    }

    public MyList(MyList nextObject, String text, int index) {
        this.nextObject = nextObject;
        this.text = text;
        this.index = index;
    }

    public void showList() {
        if (nextObject != null) {
            System.out.println(nextObject.index + " : " + nextObject.text);
            nextObject.showList();
        }
    }

    public void addToList(String text) {
        if (nextObject != null) {
            counter++;
            nextObject.addToList(text);
        } else {
            nextObject = new MyList(null, text, counter);
            counter = 1;
        }
    }

    public void addInTheMiddle(String text, int where) {
        try {
            if (nextObject.index == where) {
                nextObject = new MyList(this.getNextObject(), text, where);
                nextObject.updateIndexes();
            } else {
                nextObject.addInTheMiddle(text, where);
            }
        } catch (Exception e) {
            System.out.println("List size exceeded.");
        }
    }

    private void updateIndexes() {
        if (nextObject != null) {
            nextObject.index = nextObject.index + 1;
            nextObject.updateIndexes();
        }
    }

    public void remove(int elementNumber) {
        try {
            if (this.index == elementNumber) {
                this.text = nextObject.getText();
                this.index = nextObject.getIndex();
                this.nextObject = nextObject.getNextObject();
                this.adjustIndexes();
            } else {
                nextObject.remove(elementNumber);
            }
        } catch (Exception e) {
            System.out.println("List size exceeded.");
        }
    }

    private void adjustIndexes() {
        if (this.nextObject != null) {
            this.index = this.index - 1;
            nextObject.adjustIndexes();
        } else {
            this.index = this.index -1;
        }
    }


    public MyList getNextObject() {
        return nextObject;
    }

    public boolean hasNext() {
        return nextObject != null;
    }

    public void setNextObject(MyList nextObject) {
        this.nextObject = nextObject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Element: " + text + " ; ";
    }
}
